package ordination;

import java.time.LocalDate;
import java.time.LocalTime;

public class DagligFast extends Ordination {
	private double morgenAntal;
	private double middagAntal;
	private double aftenAntal;
	private double natAntal;
	Dosis[] doser = new Dosis[4];

	public DagligFast(LocalDate startdate, LocalDate slutdate, double morgenAntal, double middagAntal,
			double aftenAntal, double natAntal) {
		super(startdate, slutdate);

		doser[0] = new Dosis(LocalTime.of(8, 0), morgenAntal);
		doser[1] = new Dosis(LocalTime.of(12, 0), middagAntal);
		doser[2] = new Dosis(LocalTime.of(18, 0), aftenAntal);
		doser[3] = new Dosis(LocalTime.of(22, 0), natAntal);
	}

	@Override
	public double samletDosis() {
		return this.doegnDosis() * antalDage();
	}

	@Override
	public double doegnDosis() {
		double sum = 0;
		for (int i = 0; i < doser.length; i++) {
			if (doser[i].getAntal() > 0)
				sum += doser[i].getAntal();
		}
		return sum;
	}

	@Override
	public String getType() {
		return "Daglig Fast";
	}

	public Dosis[] getDoser() {
		return doser;
	}

}
