package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination {

	private double antalEnheder;
	private ArrayList<LocalDate> datoListe = new ArrayList<>();

	public PN(LocalDate startDate, LocalDate slutDate, double antal) {
		super(startDate, slutDate);
		this.antalEnheder = antal;
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true
	 * hvis givesDen er inden for ordinationens gyldighedsperiode og datoen
	 * huskes Retrurner false ellers og datoen givesDen ignoreres
	 *
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDato) {
		if (givesDato.isAfter(getStartDen()) || givesDato.isEqual(getStartDen())) {
			if (givesDato.isBefore(getSlutDen()) || givesDato.isEqual(getSlutDen())) {
				datoListe.add(givesDato);
				return true;
			}
			return false;
		} else {
			return false;
		}
	}

	@Override
	public double doegnDosis() {
		int days = (int) ChronoUnit.DAYS.between(getStartDen(), getSlutDen());

		return samletDosis() / days;
	}

	@Override
	public double samletDosis() {

		return this.getAntalGangeGivet() * this.getAntalEnheder();
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 *
	 * @return
	 */
	public int getAntalGangeGivet() {
		return datoListe.size();
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public Laegemiddel getLaegemiddel() {
		return super.getLaegemiddel();
	}

	@Override
	public String getType() {
		return "PN";
	}

}