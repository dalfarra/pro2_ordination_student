package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {

	ArrayList<Dosis> doser = new ArrayList<>();

	public DagligSkaev(LocalDate startdate, LocalDate slutdate) {
		super(startdate, slutdate);
	}

	@Override
	public double samletDosis() {
		double sum = 0;
		for (int i = 0; i < doser.size(); i++) {
			sum += doser.get(i).getAntal();
		}
		return sum * antalDage();
	}

	@Override
	public double doegnDosis() {
		return samletDosis() / antalDage();
	}

	@Override
	public String getType() {
		return "Daglig Skaev";
	}

	public void OpretDosis(LocalTime tid, double antal) {
		Dosis dosis = new Dosis(tid, antal);
		doser.add(dosis);
	}

	public ArrayList<Dosis> getDoser() {
		return new ArrayList<>(doser);
	}
}
