package Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import ordination.PN;

import org.junit.Test;

public class PNTest {

	@Test
	public void TC1GivDosis() {
		PN pn = new PN(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), 0);
		assertTrue(pn.givDosis(LocalDate.of(2015, 1, 4)));
	}

	@Test
	public void TC2GivDosis() {
		PN pn = new PN(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), 0);
		assertFalse(pn.givDosis(LocalDate.of(2015, 2, 1)));
	}

	@Test
	public void TC3SamletDosis() {
		PN pn1 = new PN(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), 4);
		pn1.givDosis(LocalDate.of(2015, 1, 3));
		pn1.givDosis(LocalDate.of(2015, 1, 4));
		pn1.givDosis(LocalDate.of(2015, 1, 6));
		pn1.givDosis(LocalDate.of(2015, 1, 7));
		pn1.givDosis(LocalDate.of(2015, 1, 10));

		assertEquals(20, pn1.samletDosis(), 0.001);

	}

	@Test
	public void TC4oegnDosis() {
		PN pn1 = new PN(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 6), 4);
		pn1.givDosis(LocalDate.of(2015, 1, 3));
		pn1.givDosis(LocalDate.of(2015, 1, 4));
		pn1.givDosis(LocalDate.of(2015, 1, 6));
		pn1.givDosis(LocalDate.of(2015, 1, 7));
		pn1.givDosis(LocalDate.of(2015, 1, 10));
		pn1.doegnDosis();

	}
}
