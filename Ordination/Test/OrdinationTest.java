package Test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.PN;
import ordination.Patient;
import service.Service;
import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;

public class OrdinationTest
{
	private Laegemiddel lm = new Laegemiddel("Flopper", 2, 3, 5, "kanyler");
	private Patient patient = new Patient("02-02-2002", "Anders Blaaberg Clausen", 37);
	private LocalTime[] klokkesletTest = {LocalTime.of(12, 05), LocalTime.of(12, 05), LocalTime.of(12, 05)};
	private double[] antalEnhederTest = {3, 6, 7};
	
	@Before


	@Test
	public void antalDageTestPNoneDay()
	{
		PN pn = new PN(LocalDate.of(2015, 10, 17), LocalDate.of(2015, 10, 17), 1);
		assertEquals(1, pn.antalDage());
	}
	
	@Test
	public void antalDageTestPNsevenDays()
	{
		PN pn = new PN(LocalDate.of(2015, 10, 17), LocalDate.of(2015, 10, 24), 1);
		assertEquals(8, pn.antalDage());
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void antalDageTestPNNegativ()
	{
		Service service = Service.getTestService();
		PN pn = service.opretPNOrdination(LocalDate.of(2015, 10, 18), LocalDate.of(2015, 10, 10), patient, lm, 3);
		pn.antalDage();
	}
	
	@Test
	public void antalDageTestDagligFastOneDay()
	{
		Service service = Service.getTestService();
		DagligFast df = service.opretDagligFastOrdination(LocalDate.of(2015, 10, 17), LocalDate.of(2015, 10, 17), patient, lm, 3, 1, 7, 8);
		assertEquals(1, df.antalDage());
	}
	
	@Test
	public void antalDageTestDagligFastSevenDays()
	{
		Service service = Service.getTestService();
		DagligFast df = service.opretDagligFastOrdination(LocalDate.of(2015, 10, 10), LocalDate.of(2015, 10, 17), patient, lm, 3, 1, 7, 8);
		assertEquals(8, df.antalDage());
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void antalDageTestDagligFastNegativ()
	{
		Service service = Service.getTestService();
		DagligFast df = service.opretDagligFastOrdination(LocalDate.of(2015, 10, 17), LocalDate.of(2015, 10, 10), patient, lm, 3, 1, 0, 3);
		df.antalDage();
	}
	
	@Test
	public void antalDageTestDagligSkaevOneDay()
	{
		Service service = Service.getTestService();
		DagligSkaev ds = service.opretDagligSkaevOrdination(LocalDate.of(2015, 10, 10), LocalDate.of(2015, 10, 10), patient, lm, klokkesletTest, antalEnhederTest);
		assertEquals(1, ds.antalDage());
	}
	
	@Test
	public void antalDageTestDagligSkaevSevenDays()
	{
		Service service = Service.getTestService();
		DagligSkaev ds = service.opretDagligSkaevOrdination(LocalDate.of(2015, 10, 10), LocalDate.of(2015, 10, 17), patient, lm, klokkesletTest, antalEnhederTest);
		assertEquals(8, ds.antalDage());
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void antalDageTestDagligSkaevNegativ()
	{
		Service service = Service.getTestService();
		DagligSkaev ds = service.opretDagligSkaevOrdination(LocalDate.of(2015, 10, 17), LocalDate.of(2015, 10, 10), patient, lm, klokkesletTest, antalEnhederTest);
		ds.antalDage();
	}
}
