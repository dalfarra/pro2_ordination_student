package Test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

import ordination.DagligFast;

public class DagligFastTest {

	@Test
	public void TC1DoegnDosis() {
		DagligFast df1 = new DagligFast(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), 2, 1, 1, 2);
		assertEquals(6, df1.doegnDosis(), 0.001);
	}

	@Test
	public void TC2DoegnDosis() {
		DagligFast df2 = new DagligFast(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), 0, 0, 0, 0);
		assertEquals(0, df2.doegnDosis(), 0.001);
	}

	@Test
	public void TC3SamletDosis() {
		DagligFast df3 = new DagligFast(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 2), 5, 4, 6, 2);
		assertEquals(34, df3.samletDosis(), 0.001);
	}
}
