package Test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import org.junit.Test;

import ordination.DagligSkaev;
import ordination.Dosis;

public class DagligSkaevTest {

	@Test
	public void DagligSkaevTestSamletDosis() {
		DagligSkaev DS = new DagligSkaev(LocalDate.of(2016, 01, 01), LocalDate.of(2016, 01, 02));
		DS.OpretDosis(LocalTime.of(20, 10), 4);
		DS.OpretDosis(LocalTime.of(10, 20), 2);
		DS.OpretDosis(LocalTime.of(20, 10), 6);
		assertEquals(24, DS.samletDosis(), 0.001);

	}

	@Test
	public void DagligSkaevTestDoegnDosis() {
		DagligSkaev DS = new DagligSkaev(LocalDate.of(2016, 01, 01), LocalDate.of(2016, 01, 02));
		DS.OpretDosis(LocalTime.of(20, 10), 4);
		DS.OpretDosis(LocalTime.of(10, 20), 2);
		DS.OpretDosis(LocalTime.of(20, 10), 6);
		assertEquals(12, DS.doegnDosis(), 0.001);
	}

	@Test
	public void DagligSkaevTestopretDosis() {
		DagligSkaev DS = new DagligSkaev(LocalDate.of(2016, 01, 01), LocalDate.of(2016, 01, 02));
		DS.OpretDosis(LocalTime.of(12, 00), 4);
		DS.OpretDosis(LocalTime.of(14, 00), 4);

		Dosis d1 = new Dosis(LocalTime.of(12, 00), 4);
		Dosis d2 = new Dosis(LocalTime.of(14, 00), 4);
		ArrayList<Dosis> list = new ArrayList<>();
		list.add(d1);
		list.add(d2);
		for (int i = 0; i < list.size(); i++) {
			assertEquals(list.get(i).getTid(), DS.getDoser().get(i).getTid());
			assertEquals(list.get(i).getAntal(), DS.getDoser().get(i).getAntal(), 0.0001);
		}
	}

}
