package Test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.Laegemiddel;
import ordination.Patient;
import service.Service;

public class ServiceTest {
	private Service s = Service.getTestService();
	private Laegemiddel paracetamol = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
	private Laegemiddel methotrexat = new Laegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk");
	private Laegemiddel fucidin = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

	@Before
	public void setup() {
		s.createSomeObjects();
	}

	@Test
	public void anbefaletDosisPrDoegnLetTest() {
		Patient P1 = new Patient("Jane Jensen", "121256-0512", 24);
		assertEquals(24.0, s.anbefaletDosisPrDoegn(P1, paracetamol), 0.0001);
	}

	@Test
	public void anbefaletDosisPrDoegnNormalGrænseværdiTest() {
		Patient P1 = new Patient("Jane Jensen", "121256-0512", 25);
		assertEquals(37.5, s.anbefaletDosisPrDoegn(P1, paracetamol), 0.0001);
	}

	@Test
	public void anbefaletDosisPrDoegnTungGrænseværdiTest() {
		Patient P1 = new Patient("Jane Jensen", "121256-0512", 120);
		assertEquals(180, s.anbefaletDosisPrDoegn(P1, paracetamol), 0.0001);
	}

	@Test
	public void anbefaletDosisPrDoegnTungTest() {
		Patient P1 = new Patient("Jane Jensen", "121256-0512", 195);
		assertEquals(390, s.anbefaletDosisPrDoegn(P1, paracetamol), 0.0001);
	}

	@Test(expected = IllegalArgumentException.class)
	public void anbefaletDosisPrDoegnExceptionTest() {
		Patient P1 = new Patient("Jane Jensen", "121256-0512", -2);
		s.anbefaletDosisPrDoegn(P1, paracetamol);
	}

	@Test(expected = IllegalArgumentException.class)
	public void ordinationPNAnvendtInvalidDateTest() {
		Service s = Service.getTestService();
		Patient P1 = new Patient("Jane Jensen", "121256-0512", 60);
		s.ordinationPNAnvendt(
				s.opretPNOrdination(LocalDate.of(2015, 01, 01), LocalDate.of(2015, 01, 05), P1, paracetamol, 20),
				LocalDate.of(2015, 01, 07));

	}

	@Test
	public void ordinationPNAnvendtValidDateTest() {
		Patient P1 = new Patient("Jane Jensen", "121256-0512", 60);
		s.ordinationPNAnvendt(
				s.opretPNOrdination(LocalDate.of(2015, 01, 01), LocalDate.of(2015, 01, 05), P1, paracetamol, 20),
				LocalDate.of(2015, 01, 04));
	}

	@Test
	public void antalOrdinationerPrVægtPrLægemiddelNedreGrænseTest() {

		assertEquals(0, s.antalOrdinationerPrVægtPrLægemiddel(0, 20, paracetamol));
	}

	@Test
	public void antalOrdinationerPrVægtPrLægemiddelTest1() {
		assertEquals(0, s.antalOrdinationerPrVægtPrLægemiddel(0, 20, s.getAllLaegemidler().get(1)));
	}

	@Test
	public void antalOrdinationerPrVægtPrLægemiddelTest2() {
		assertEquals(1, s.antalOrdinationerPrVægtPrLægemiddel(20, 70, s.getAllLaegemidler().get(2)));
	}

	@Test
	public void antalOrdinationerPrVægtPrLægemiddelTest3() {
		assertEquals(1, s.antalOrdinationerPrVægtPrLægemiddel(80, 100, s.getAllLaegemidler().get(1)));
	}

	@Test(expected = IllegalArgumentException.class)
	public void antalOrdinationerPrVægtPrLægemiddelTest4() {
		s.antalOrdinationerPrVægtPrLægemiddel(100, 60, s.getAllLaegemidler().get(1));
	}

	@Test(expected = IllegalArgumentException.class)
	public void antalOrdinationerPrVægtPrLægemiddelTest5() {
		s.antalOrdinationerPrVægtPrLægemiddel(-2, 60, s.getAllLaegemidler().get(1));
	}

	@Test
	public void TyperTestPN() {
		assertEquals("PN", s.getAllPatienter().get(0).getOrdinationer().get(1).getType());
	}

	@Test
	public void TyperTestDagligSkaev() {
		assertEquals("Daglig Skaev", s.getAllPatienter().get(1).getOrdinationer().get(1).getType());
	}

	@Test
	public void TyperTestDagligFast() {
		assertEquals("Daglig Fast", s.getAllPatienter().get(1).getOrdinationer().get(0).getType());
	}

}
